﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiLayer.Models
{
    public class BookRequestModel
    {
        
        public string Title { get; set; }
         

        public string Author { get; set; }
    }
}

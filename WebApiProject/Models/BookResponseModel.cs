﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiLayer.Models
{
    public class BookResponseModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }

        public Guid libraryId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ApiLayer.Models;
using BusinessLayer;
using ApiLayer;
namespace LibraryProject.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class BookController:ControllerBase
    {
        private IBookService bookService;
        private ILibraryService libraryService;

        public BookController(IBookService bookService,ILibraryService libraryService)
        {
            this.bookService = bookService;
            this.libraryService = libraryService;
        }

        [HttpGet()]
        public IActionResult Get([FromQuery] bool ascendingOrder = true)
        {
            var books = bookService.GetBooks(ascendingOrder);

            return Ok(books.Select(x => Mapper.MapToResponseModel(x)));
        }

        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var book = bookService.GetBook(id);

            if (book == null)
            {
                return NotFound();
            }

            return Ok(Mapper.MapToResponseModel(book));
        }
        [HttpGet("libraryId={id}")]
        public IActionResult GetByLibrary([FromBody]Guid id)
        {
            var book = bookService.GetBookByLib(id);
             

            if (book == null)
            {
                return NotFound();
            }

            return Ok(Mapper.MapToResponseModel(book));
        }

        [HttpPost]
        public IActionResult Post([FromBody] BookRequestModel book)
        {
            var createdId = bookService.AddBook(Mapper.MapToWriteModel(book));

            return Created($"api/library/{createdId}", createdId);
        }

        [HttpPut("{id}")]
        public IActionResult Put(Guid id, [FromBody] BookRequestModel book)
        {
            var foundElement = bookService.UpdateBook(id, Mapper.MapToWriteModel(book));
            if (foundElement)
            {
                return Ok();
            }

            return NotFound();
        }

        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {

            bookService.DeleteBook(id);
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using BusinessLayer.Models;
using DataLayer;

namespace BusinessLayer
{
  public  class BookService:IBookService
    {
        private readonly IBookRepository bookRepository;
        private ILibraryRepository libraryRepository;

        public BookService()
        {
            this.bookRepository = new BookRepository();
            
            
        }

        public Guid AddBook(BusinessLayer.Models.BookWriteModel book)
        {
            if (bookRepository.GetBookTitle(book.Title))
            {
                msgSameTitle();

            }
            
            var dataModelBook = new DataLayer.Models.Books
            {
                Id = Guid.NewGuid(),
                Title = book.Title,
                Author = book.Author,
                libraryId = Guid.NewGuid()

            };
            if (libraryRepository.GetLibrary(dataModelBook.libraryId)==null)
            {
                msgLibNotFound();
            }

            bookRepository.AddBook(dataModelBook);


            return dataModelBook.Id;
            

        }

        public HttpResponseMessage msgSameTitle()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.BadRequest;
            response.Content =new StringContent( "Book with the same title already exists!");
            return response;

        }
        public HttpResponseMessage msgLibNotFound()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.NotFound;
            response.Content = new StringContent("Library id not found.You can only add books to an existing library!");
            return response;

        }

        private BookReadModel Map(DataLayer.Models.Books dataModelBook)
        {
            return new BookReadModel
            {
                Id = dataModelBook.Id,
                Title = dataModelBook.Title,
                Author = dataModelBook.Author,
                libraryId = dataModelBook.libraryId
            };
        }

        private DataLayer.Models.Books Map(Guid id, BookWriteModel bookWriteModel)
        {
            return new DataLayer.Models.Books
            {
                Id = id,
                Title = bookWriteModel.Title,
                Author = bookWriteModel.Author,
                
            };
        }

        public IEnumerable<BookReadModel> GetLibraries(bool ascendingOrder)
        {
            var dataModelLibraries = bookRepository.GetBooks().Select(x => Map(x));
            if (ascendingOrder)
            {
                return dataModelLibraries.OrderBy(x => x.Title);
            }

            return dataModelLibraries.OrderByDescending(x => x.Title);
        }

        public BookReadModel GetBook(Guid id)
        {
            var book = bookRepository.GetBook(id);
            if (book == null) return null;

            return Map(bookRepository.GetBook(id));
        }
        public BookReadModel GetBookByLib(Guid id)
        {
            var book = bookRepository.GetBookByLib(id);
            if (book == null) return null;

            return Map(bookRepository.GetBookByLib(id));
        }

        public bool UpdateBook(Guid id, BookWriteModel book)
        {
            var entityExists = bookRepository.UpdateBook(Map(id,book));
            return entityExists;
        }

        public IEnumerable<BookReadModel> GetBooks(bool ascendingOrder)
        {
            var dataModelBooks = bookRepository.GetBooks().Select(x => Map(x));
            if (ascendingOrder)
            {
                return dataModelBooks.OrderBy(x => x.Title);
            }

            return dataModelBooks.OrderByDescending(x => x.Title);
        }

        public BookReadModel GetLibrary(Guid id)
        {
            var library = bookRepository.GetBook(id);
            if (library == null) return null;

            return Map(bookRepository.GetBook(id));
        }

        public void DeleteBook(Guid id)
        {
            bookRepository.DeleteBook(id);
        }

         
    }
}

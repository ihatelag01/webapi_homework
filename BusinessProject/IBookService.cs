﻿using System;
using System.Collections.Generic;
using System.Text;
using BusinessLayer.Models;

namespace BusinessLayer
{
   public interface IBookService
    {
        public Guid AddBook(BookWriteModel book);

        public IEnumerable<BookReadModel> GetBooks(bool ascendingOrder);

        public BookReadModel GetBook(Guid id);

        public bool UpdateBook(Guid id, BookWriteModel book);

        public void DeleteBook(Guid id);

        public BookReadModel GetBookByLib(Guid id);
    }
}

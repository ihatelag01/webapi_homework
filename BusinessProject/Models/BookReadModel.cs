﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Models
{
  public  class BookReadModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public Guid libraryId { get; set; }
    }
}

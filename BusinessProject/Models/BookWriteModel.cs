﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Models
{
  public class BookWriteModel
    {
        public string Author { get; set; }
        public string Title { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using DataLayer.Models;

namespace DataLayer
{
    public interface IBookRepository
    {
        IEnumerable<Books> GetBooks();

        void AddBook(Books book);

        Books GetBook(Guid id);

        bool UpdateBook(Books book);

        bool GetBookTitle(string title);

        void DeleteBook(Guid id);

        Books GetBookByLib(Guid id);
    }
}

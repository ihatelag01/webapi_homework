﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer.Models;

namespace DataLayer
{
   public class BookRepository : IBookRepository
    {
        public List<Books> Books { get; }

        public BookRepository()
        {
            Books = new List<Books>
            {
                new Books()
                {
                    Title = "Pet Sematary",
                    Author="Stephen King",
                    Id=Guid.Parse("e4a6c6c5-d96a-4651-b9a2-6605ac98fb17"),
                    libraryId = Guid.Parse("97338924-a1b6-4fa2-9f1a-992e2376a291")

                },

                new Books()
                {
                Title = "And then there were none",
                Author="Agatha Christie",
                Id=Guid.Parse("e4a6c6c5-d96a-4651-b9a2-6605ac98fb17"),
                libraryId = Guid.Parse("5f6fdf50-6e23-432b-9f29-9e19e18d26c1")

            }



            };
        }

        public IEnumerable<Books> GetBooks()
        {
            return Books;
        }

        public Books GetBook(Guid id)
        {
            return Books.FirstOrDefault(x => x.Id == id);
        }

        public Books GetBookByLib(Guid libId)
        {
            return Books.FirstOrDefault(x => x.libraryId == libId);
        }

        public void AddBook(Books book)
        {
            Books.Add(book);
        }

        public bool UpdateBook(Books book)
        {
            var index = Books.FindIndex(x => x.Id == book.Id);
            if (index == -1)
            {
                return false;
            }

            Books[index] = book;

            return true;
        }

        public bool GetBookTitle(string title)
        {
            foreach (var book in Books)
            {
                if (book.Title == title)
                    return true;
            }

            return false;
        }

        public void DeleteBook(Guid id)
        {
            var book = Books.Find(x => x.Id == id);
            Books.Remove(book);

        }


    }
}
